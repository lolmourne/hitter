package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

func main() {

	ticker := time.NewTicker(time.Millisecond * 200)
	for {
		select {
		case <-ticker.C:
			url := "https://chat.tokopedia.com/gcn/api/v3/channel/1038/buttons"

			req, _ := http.NewRequest("GET", url, nil)

			req.Header.Add("cache-control", "no-cache")
			req.Header.Add("Postman-Token", "d379588a-6b4e-401b-ba7b-dd2b3754f50d")

			res, _ := http.DefaultClient.Do(req)

			defer res.Body.Close()
			body, _ := ioutil.ReadAll(res.Body)

			fmt.Println(res)
			fmt.Println(string(body))
		}
	}

}
