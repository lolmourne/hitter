package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

func main() {

	ticker := time.NewTicker(time.Millisecond * 200)
	for {
		select {
		case <-ticker.C:
			url := "https://chat.tokopedia.com/gmf/api/v2/trivia?campaign=semarak-ramadan-ekstra-1"

			req, _ := http.NewRequest("GET", url, nil)

			req.Header.Add("X-User-Token", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjo1NTc4MjU5LCJpZCI6NTU3ODI1OSwibmFtZSI6IkFkcmlhbnVzIEtldmluIiwiZGF0YSI6IiIsImF1ZCI6IkFkcmlhbnVzIEtldmluIiwiZXhwIjoxNTU3ODIyMTAzLCJqdGkiOiI1NTc4MjU5IiwiaWF0IjoxNTU3ODE3OTAzLCJpc3MiOiJ0b2tvcGVkaWFfcGxheSIsIm5iZiI6MTU1NzgxNzkwMywic3ViIjoidG9rb3BlZGlhX3BsYXlfdG9rZW5fNTU3ODI1OV8xNTU3ODE3OTAzIn0.sIPN76_eJWB-Wyme5puwX07emJbXVRC8GiySgItBfE4")
			req.Header.Add("cache-control", "no-cache")
			req.Header.Add("Postman-Token", "d338202b-47de-4104-a6d0-f36f6442732c")

			res, _ := http.DefaultClient.Do(req)

			defer res.Body.Close()
			body, _ := ioutil.ReadAll(res.Body)

			fmt.Println(res)
			fmt.Println(string(body))
		}
	}

}
