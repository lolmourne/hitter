run-button: button-build buton-start

button-build:
	@echo " >> building binaries"
	@go build -o bin/hitter button/main.go

buton-start:
	@echo " >> starting binaries"
	@./bin/hitter

run-trivia: trivia-build trivia-start

trivia-build:
	@echo " >> building binaries"
	@go build -o bin/trivia trivia/main.go

trivia-start:
	@echo " >> starting binaries"
	@./bin/trivia

run-vote: vote-build vote-start

vote-build:
	@echo " >> building binaries"
	@go build -o bin/vote vote/main.go

vote-start:
	@echo " >> starting binaries"
	@./bin/vote


run-channel: run-build run-start

run-build:
	@echo " >> building binaries"
	@go build -o bin/channel channel/main.go

run-start:
	@echo " >> starting binaries"
	@./bin/channel