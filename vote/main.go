package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

func main() {

	ticker := time.NewTicker(time.Millisecond * 200)
	for {
		select {
		case <-ticker.C:
			url := "https://chat.tokopedia.com/gmf/api/v3/channel/927/quiz"

			req, _ := http.NewRequest("GET", url, nil)

			res, _ := http.DefaultClient.Do(req)

			defer res.Body.Close()
			body, _ := ioutil.ReadAll(res.Body)

			fmt.Println(res)
			fmt.Println(string(body))
		}
	}

}
